"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Job = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _nquirer = require("nquirer");

var _winston = require("winston");

var _winston2 = _interopRequireDefault(_winston);

var _shelljs = require("shelljs");

var _shelljs2 = _interopRequireDefault(_shelljs);

var _nodeWatch = require("node-watch");

var _nodeWatch2 = _interopRequireDefault(_nodeWatch);

var _jsYaml = require("js-yaml");

var _jsYaml2 = _interopRequireDefault(_jsYaml);

var _tmp = require("tmp");

var _tmp2 = _interopRequireDefault(_tmp);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _argv = require("../config/argv.json");

var _argv2 = _interopRequireDefault(_argv);

var _defaultConfig = require("../config/default-config.json");

var _defaultConfig2 = _interopRequireDefault(_defaultConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Job = exports.Job = function () {
  function Job() {
    _classCallCheck(this, Job);
  }

  _createClass(Job, [{
    key: "configure",
    value: function configure() {
      // Default nconf configuration
      // https://github.com/indexzero/nconf
      _nquirer.nconf.argv(_argv2.default).env().defaults(_defaultConfig2.default).file("default-config.json");

      // Logging
      // https://github.com/winstonjs/winston#logging-levels
      _winston2.default.level = _nquirer.nconf.get("logLevel");
    }
  }, {
    key: "run",
    value: function run() {
      // Prompt for missing configurations and continue with application logic...
      var tmpMockFile = _tmp2.default.fileSync();
      var mocksFolder = void 0;

      function createMockFile(mocksFolder) {
        var mocks = _path2.default.join(mocksFolder, "**/*.{yaml,json,js}");

        var datas = _shelljs2.default.ls(mocks).map(function (filepath) {
          if (/\.yaml$/.test(filepath)) {
            try {
              return _jsYaml2.default.load(_shelljs2.default.cat(filepath));
            } catch (e) {
              console.error(e);
              return null;
            }
          }
          if (/\.(js|json)$/.test(filepath)) {
            try {
              return require(filepath);
            } catch (e) {
              console.error(e);
              return null;
            }
          }
          return null;
        }).filter(function (str) {
          return str !== null;
        });
        _fs2.default.writeFileSync(tmpMockFile.name, JSON.stringify(datas), "utf8");
        return datas;
      }

      return (0, _nquirer.inquire)().then(function (nconf) {
        // verify mock folder
        var mocks = nconf.get("mocks");
        console.log(process.cwd());
        mocksFolder = _path2.default.join(process.cwd(), mocks);
        console.log("mocksfolder", mocksFolder);
        if (!_fs2.default.existsSync(mocksFolder)) {
          _winston2.default.error("There is no folder " + mocksFolder);
          process.exit(1);
        }
        var Stubby = require("stubby").Stubby;
        var stubby = new Stubby();

        stubby.start({
          data: createMockFile(mocksFolder),
          persistent: true,
          quiet: nconf.get("quiet"),
          watch: tmpMockFile.name,
          admin: nconf.get("admin"),
          cert: nconf.get("cert"),
          key: nconf.get("key"),
          location: nconf.get("location"),
          pfx: nconf.get("pfx"),
          stubs: nconf.get("stubs"),
          tls: nconf.get("tls")
        });

        (0, _nodeWatch2.default)(mocksFolder, { recursive: true }, function (evt, name) {
          var cleanedName = name.replace(/\.(js|json|yaml).+?$/, ".$1");
          if (name == cleanedName) {
            console.log("Stubby-simply mock changed: ", cleanedName);
            delete require.cache[require.resolve(cleanedName)];
          }
          createMockFile(mocksFolder);
        });

        return "ok";
      });
    }
  }]);

  return Job;
}();

var instance = new Job();
exports.default = instance;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zdHViYnktc2ltcGx5LmpzIl0sIm5hbWVzIjpbIkpvYiIsImFyZ3YiLCJlbnYiLCJkZWZhdWx0cyIsImZpbGUiLCJsZXZlbCIsImdldCIsInRtcE1vY2tGaWxlIiwiZmlsZVN5bmMiLCJtb2Nrc0ZvbGRlciIsImNyZWF0ZU1vY2tGaWxlIiwibW9ja3MiLCJqb2luIiwiZGF0YXMiLCJscyIsIm1hcCIsInRlc3QiLCJmaWxlcGF0aCIsImxvYWQiLCJjYXQiLCJlIiwiY29uc29sZSIsImVycm9yIiwicmVxdWlyZSIsImZpbHRlciIsInN0ciIsIndyaXRlRmlsZVN5bmMiLCJuYW1lIiwiSlNPTiIsInN0cmluZ2lmeSIsInRoZW4iLCJuY29uZiIsImxvZyIsInByb2Nlc3MiLCJjd2QiLCJleGlzdHNTeW5jIiwiZXhpdCIsIlN0dWJieSIsInN0dWJieSIsInN0YXJ0IiwiZGF0YSIsInBlcnNpc3RlbnQiLCJxdWlldCIsIndhdGNoIiwiYWRtaW4iLCJjZXJ0Iiwia2V5IiwibG9jYXRpb24iLCJwZngiLCJzdHVicyIsInRscyIsInJlY3Vyc2l2ZSIsImV2dCIsImNsZWFuZWROYW1lIiwicmVwbGFjZSIsImNhY2hlIiwicmVzb2x2ZSIsImluc3RhbmNlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7Ozs7O0lBRWFBLEcsV0FBQUEsRzs7Ozs7OztnQ0FDQztBQUNWO0FBQ0E7QUFDQSxxQkFDR0MsSUFESCxpQkFFR0MsR0FGSCxHQUdHQyxRQUhILDBCQUlHQyxJQUpILENBSVEscUJBSlI7O0FBTUE7QUFDQTtBQUNBLHdCQUFRQyxLQUFSLEdBQWdCLGVBQU1DLEdBQU4sQ0FBVSxVQUFWLENBQWhCO0FBQ0Q7OzswQkFFSztBQUNKO0FBQ0EsVUFBSUMsY0FBYyxjQUFJQyxRQUFKLEVBQWxCO0FBQ0EsVUFBSUMsb0JBQUo7O0FBRUEsZUFBU0MsY0FBVCxDQUF3QkQsV0FBeEIsRUFBcUM7QUFDbkMsWUFBSUUsUUFBUSxlQUFLQyxJQUFMLENBQVVILFdBQVYsRUFBdUIscUJBQXZCLENBQVo7O0FBRUEsWUFBSUksUUFBUSxrQkFDVEMsRUFEUyxDQUNOSCxLQURNLEVBRVRJLEdBRlMsQ0FFTCxvQkFBWTtBQUNmLGNBQUksVUFBVUMsSUFBVixDQUFlQyxRQUFmLENBQUosRUFBOEI7QUFDNUIsZ0JBQUk7QUFDRixxQkFBTyxpQkFBT0MsSUFBUCxDQUFZLGtCQUFNQyxHQUFOLENBQVVGLFFBQVYsQ0FBWixDQUFQO0FBQ0QsYUFGRCxDQUVFLE9BQU9HLENBQVAsRUFBVTtBQUNWQyxzQkFBUUMsS0FBUixDQUFjRixDQUFkO0FBQ0EscUJBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRCxjQUFJLGVBQWVKLElBQWYsQ0FBb0JDLFFBQXBCLENBQUosRUFBbUM7QUFDakMsZ0JBQUk7QUFDRixxQkFBT00sUUFBUU4sUUFBUixDQUFQO0FBQ0QsYUFGRCxDQUVFLE9BQU9HLENBQVAsRUFBVTtBQUNWQyxzQkFBUUMsS0FBUixDQUFjRixDQUFkO0FBQ0EscUJBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRCxpQkFBTyxJQUFQO0FBQ0QsU0FwQlMsRUFxQlRJLE1BckJTLENBcUJGO0FBQUEsaUJBQU9DLFFBQVEsSUFBZjtBQUFBLFNBckJFLENBQVo7QUFzQkEscUJBQUdDLGFBQUgsQ0FBaUJuQixZQUFZb0IsSUFBN0IsRUFBbUNDLEtBQUtDLFNBQUwsQ0FBZWhCLEtBQWYsQ0FBbkMsRUFBMEQsTUFBMUQ7QUFDQSxlQUFPQSxLQUFQO0FBQ0Q7O0FBRUQsYUFBTyx3QkFBVWlCLElBQVYsQ0FBZSxpQkFBUztBQUM3QjtBQUNBLFlBQUluQixRQUFRb0IsTUFBTXpCLEdBQU4sQ0FBVSxPQUFWLENBQVo7QUFDQWUsZ0JBQVFXLEdBQVIsQ0FBWUMsUUFBUUMsR0FBUixFQUFaO0FBQ0F6QixzQkFBYyxlQUFLRyxJQUFMLENBQVVxQixRQUFRQyxHQUFSLEVBQVYsRUFBeUJ2QixLQUF6QixDQUFkO0FBQ0FVLGdCQUFRVyxHQUFSLENBQVksYUFBWixFQUEyQnZCLFdBQTNCO0FBQ0EsWUFBSSxDQUFDLGFBQUcwQixVQUFILENBQWMxQixXQUFkLENBQUwsRUFBaUM7QUFDL0IsNEJBQVFhLEtBQVIsQ0FBYyx3QkFBd0JiLFdBQXRDO0FBQ0F3QixrQkFBUUcsSUFBUixDQUFhLENBQWI7QUFDRDtBQUNELFlBQUlDLFNBQVNkLFFBQVEsUUFBUixFQUFrQmMsTUFBL0I7QUFDQSxZQUFJQyxTQUFTLElBQUlELE1BQUosRUFBYjs7QUFFQUMsZUFBT0MsS0FBUCxDQUFhO0FBQ1hDLGdCQUFNOUIsZUFBZUQsV0FBZixDQURLO0FBRVhnQyxzQkFBWSxJQUZEO0FBR1hDLGlCQUFPWCxNQUFNekIsR0FBTixDQUFVLE9BQVYsQ0FISTtBQUlYcUMsaUJBQU9wQyxZQUFZb0IsSUFKUjtBQUtYaUIsaUJBQU9iLE1BQU16QixHQUFOLENBQVUsT0FBVixDQUxJO0FBTVh1QyxnQkFBTWQsTUFBTXpCLEdBQU4sQ0FBVSxNQUFWLENBTks7QUFPWHdDLGVBQUtmLE1BQU16QixHQUFOLENBQVUsS0FBVixDQVBNO0FBUVh5QyxvQkFBVWhCLE1BQU16QixHQUFOLENBQVUsVUFBVixDQVJDO0FBU1gwQyxlQUFLakIsTUFBTXpCLEdBQU4sQ0FBVSxLQUFWLENBVE07QUFVWDJDLGlCQUFPbEIsTUFBTXpCLEdBQU4sQ0FBVSxPQUFWLENBVkk7QUFXWDRDLGVBQUtuQixNQUFNekIsR0FBTixDQUFVLEtBQVY7QUFYTSxTQUFiOztBQWNBLGlDQUFNRyxXQUFOLEVBQW1CLEVBQUUwQyxXQUFXLElBQWIsRUFBbkIsRUFBd0MsVUFBU0MsR0FBVCxFQUFjekIsSUFBZCxFQUFvQjtBQUMxRCxjQUFJMEIsY0FBYzFCLEtBQUsyQixPQUFMLENBQWEsc0JBQWIsRUFBcUMsS0FBckMsQ0FBbEI7QUFDQSxjQUFJM0IsUUFBUTBCLFdBQVosRUFBeUI7QUFDdkJoQyxvQkFBUVcsR0FBUixDQUFZLDhCQUFaLEVBQTRDcUIsV0FBNUM7QUFDQSxtQkFBTzlCLFFBQVFnQyxLQUFSLENBQWNoQyxRQUFRaUMsT0FBUixDQUFnQkgsV0FBaEIsQ0FBZCxDQUFQO0FBQ0Q7QUFDRDNDLHlCQUFlRCxXQUFmO0FBQ0QsU0FQRDs7QUFTQSxlQUFPLElBQVA7QUFDRCxPQXJDTSxDQUFQO0FBc0NEOzs7Ozs7QUFHSCxJQUFNZ0QsV0FBVyxJQUFJekQsR0FBSixFQUFqQjtrQkFDZXlELFEiLCJmaWxlIjoic3R1YmJ5LXNpbXBseS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7bmNvbmYsIGlucXVpcmV9IGZyb20gXCJucXVpcmVyXCI7XG5pbXBvcnQgd2luc3RvbiBmcm9tIFwid2luc3RvblwiO1xuXG5pbXBvcnQgc2hlbGwgZnJvbSBcInNoZWxsanNcIjtcbmltcG9ydCB3YXRjaCBmcm9tIFwibm9kZS13YXRjaFwiO1xuaW1wb3J0IGpzeWFtbCBmcm9tIFwianMteWFtbFwiO1xuaW1wb3J0IHRtcCBmcm9tIFwidG1wXCI7XG5pbXBvcnQgZnMgZnJvbSBcImZzXCI7XG5pbXBvcnQgcGF0aCBmcm9tIFwicGF0aFwiO1xuXG5pbXBvcnQgYXJndkNvbmZpZyBmcm9tIFwiLi4vY29uZmlnL2FyZ3YuanNvblwiO1xuaW1wb3J0IGRlZmF1bHRDb25maWcgZnJvbSBcIi4uL2NvbmZpZy9kZWZhdWx0LWNvbmZpZy5qc29uXCI7XG5cbmV4cG9ydCBjbGFzcyBKb2Ige1xuICBjb25maWd1cmUoKSB7XG4gICAgLy8gRGVmYXVsdCBuY29uZiBjb25maWd1cmF0aW9uXG4gICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2luZGV4emVyby9uY29uZlxuICAgIG5jb25mXG4gICAgICAuYXJndihhcmd2Q29uZmlnKVxuICAgICAgLmVudigpXG4gICAgICAuZGVmYXVsdHMoZGVmYXVsdENvbmZpZylcbiAgICAgIC5maWxlKFwiZGVmYXVsdC1jb25maWcuanNvblwiKTtcblxuICAgIC8vIExvZ2dpbmdcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vd2luc3RvbmpzL3dpbnN0b24jbG9nZ2luZy1sZXZlbHNcbiAgICB3aW5zdG9uLmxldmVsID0gbmNvbmYuZ2V0KFwibG9nTGV2ZWxcIik7XG4gIH1cblxuICBydW4oKSB7XG4gICAgLy8gUHJvbXB0IGZvciBtaXNzaW5nIGNvbmZpZ3VyYXRpb25zIGFuZCBjb250aW51ZSB3aXRoIGFwcGxpY2F0aW9uIGxvZ2ljLi4uXG4gICAgbGV0IHRtcE1vY2tGaWxlID0gdG1wLmZpbGVTeW5jKCk7XG4gICAgbGV0IG1vY2tzRm9sZGVyO1xuXG4gICAgZnVuY3Rpb24gY3JlYXRlTW9ja0ZpbGUobW9ja3NGb2xkZXIpIHtcbiAgICAgIGxldCBtb2NrcyA9IHBhdGguam9pbihtb2Nrc0ZvbGRlciwgXCIqKi8qLnt5YW1sLGpzb24sanN9XCIpO1xuXG4gICAgICBsZXQgZGF0YXMgPSBzaGVsbFxuICAgICAgICAubHMobW9ja3MpXG4gICAgICAgIC5tYXAoZmlsZXBhdGggPT4ge1xuICAgICAgICAgIGlmICgvXFwueWFtbCQvLnRlc3QoZmlsZXBhdGgpKSB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICByZXR1cm4ganN5YW1sLmxvYWQoc2hlbGwuY2F0KGZpbGVwYXRoKSk7XG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XG4gICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoL1xcLihqc3xqc29uKSQvLnRlc3QoZmlsZXBhdGgpKSB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICByZXR1cm4gcmVxdWlyZShmaWxlcGF0aCk7XG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XG4gICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfSlcbiAgICAgICAgLmZpbHRlcihzdHIgPT4gc3RyICE9PSBudWxsKTtcbiAgICAgIGZzLndyaXRlRmlsZVN5bmModG1wTW9ja0ZpbGUubmFtZSwgSlNPTi5zdHJpbmdpZnkoZGF0YXMpLCBcInV0ZjhcIik7XG4gICAgICByZXR1cm4gZGF0YXM7XG4gICAgfVxuXG4gICAgcmV0dXJuIGlucXVpcmUoKS50aGVuKG5jb25mID0+IHtcbiAgICAgIC8vIHZlcmlmeSBtb2NrIGZvbGRlclxuICAgICAgbGV0IG1vY2tzID0gbmNvbmYuZ2V0KFwibW9ja3NcIik7XG4gICAgICBjb25zb2xlLmxvZyhwcm9jZXNzLmN3ZCgpKTtcbiAgICAgIG1vY2tzRm9sZGVyID0gcGF0aC5qb2luKHByb2Nlc3MuY3dkKCksIG1vY2tzKTtcbiAgICAgIGNvbnNvbGUubG9nKFwibW9ja3Nmb2xkZXJcIiwgbW9ja3NGb2xkZXIpO1xuICAgICAgaWYgKCFmcy5leGlzdHNTeW5jKG1vY2tzRm9sZGVyKSkge1xuICAgICAgICB3aW5zdG9uLmVycm9yKFwiVGhlcmUgaXMgbm8gZm9sZGVyIFwiICsgbW9ja3NGb2xkZXIpO1xuICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICB9XG4gICAgICBsZXQgU3R1YmJ5ID0gcmVxdWlyZShcInN0dWJieVwiKS5TdHViYnk7XG4gICAgICBsZXQgc3R1YmJ5ID0gbmV3IFN0dWJieSgpO1xuXG4gICAgICBzdHViYnkuc3RhcnQoe1xuICAgICAgICBkYXRhOiBjcmVhdGVNb2NrRmlsZShtb2Nrc0ZvbGRlciksXG4gICAgICAgIHBlcnNpc3RlbnQ6IHRydWUsXG4gICAgICAgIHF1aWV0OiBuY29uZi5nZXQoXCJxdWlldFwiKSxcbiAgICAgICAgd2F0Y2g6IHRtcE1vY2tGaWxlLm5hbWUsXG4gICAgICAgIGFkbWluOiBuY29uZi5nZXQoXCJhZG1pblwiKSxcbiAgICAgICAgY2VydDogbmNvbmYuZ2V0KFwiY2VydFwiKSxcbiAgICAgICAga2V5OiBuY29uZi5nZXQoXCJrZXlcIiksXG4gICAgICAgIGxvY2F0aW9uOiBuY29uZi5nZXQoXCJsb2NhdGlvblwiKSxcbiAgICAgICAgcGZ4OiBuY29uZi5nZXQoXCJwZnhcIiksXG4gICAgICAgIHN0dWJzOiBuY29uZi5nZXQoXCJzdHVic1wiKSxcbiAgICAgICAgdGxzOiBuY29uZi5nZXQoXCJ0bHNcIilcbiAgICAgIH0pO1xuXG4gICAgICB3YXRjaChtb2Nrc0ZvbGRlciwgeyByZWN1cnNpdmU6IHRydWUgfSwgZnVuY3Rpb24oZXZ0LCBuYW1lKSB7XG4gICAgICAgIGxldCBjbGVhbmVkTmFtZSA9IG5hbWUucmVwbGFjZSgvXFwuKGpzfGpzb258eWFtbCkuKz8kLywgXCIuJDFcIik7XG4gICAgICAgIGlmIChuYW1lID09IGNsZWFuZWROYW1lKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJTdHViYnktc2ltcGx5IG1vY2sgY2hhbmdlZDogXCIsIGNsZWFuZWROYW1lKTtcbiAgICAgICAgICBkZWxldGUgcmVxdWlyZS5jYWNoZVtyZXF1aXJlLnJlc29sdmUoY2xlYW5lZE5hbWUpXTtcbiAgICAgICAgfVxuICAgICAgICBjcmVhdGVNb2NrRmlsZShtb2Nrc0ZvbGRlcik7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIFwib2tcIjtcbiAgICB9KTtcbiAgfVxufVxuXG5jb25zdCBpbnN0YW5jZSA9IG5ldyBKb2IoKTtcbmV4cG9ydCBkZWZhdWx0IGluc3RhbmNlO1xuIl19